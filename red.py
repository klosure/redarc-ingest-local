import os
import subprocess
import requests
import time
from datetime import datetime

from PIL import Image
from tinydb import TinyDB, Query

AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36'

HEADER = {'user-agent': AGENT}
BASE_URL = 'https://api.reddit.com/r'
SUB_FILE = './subreddits.conf'
DATA_DIR = './redarc-ingest-local-data'

db = TinyDB('db.json')
db_table_authors = db.table('authors')
db_table_posts = db.table('posts')


'''
SCHEMA

TABLE authors
{
    author: str
    posts: [FK]
}

TABLE posts
{
    reddit_post_id: str
    author: str
    title: str
    sub: str
    filename: str
}

'''


def load_subreddits():
    subreddits = []
    with open(SUB_FILE) as fp:
        lines = fp.readlines()
        for sub in lines:
            subreddits.append(sub.rstrip())
    return subreddits


def build_url(sub: str):
    filter_by = "new"
    return "{0}/{1}/{2}".format(BASE_URL, sub, filter_by)


def call_reddit(sub: str):
    url = build_url(sub)
    resp = requests.get(url, headers=HEADER)
    json = resp.json()["data"]["children"]
    return json


def check_posts(posts):
    for post in posts:
        data = post['data']

        if 'is_self' not in data:
            print("Did you enter an invalid subreddit name? Double check the spelling on that...")

        if data['is_self'] is True:
            continue

        author = data['author']
        title = data['title']
        reddit_post_id = data['id']
        thumb_url = data['thumbnail']
        sub = data['subreddit']

        is_gallery = False
        if 'is_gallery' in data:
            is_gallery = data['is_gallery']

        if is_author_unsaved(author):
            create_author(author)
        if is_post_unsaved(reddit_post_id):
            if is_gallery:
                save_gallery(author, title, data)
            else:
                if data['is_video']:
                    save_video_post(author, title, data)
                else:
                    save_post(author, title, thumb_url, data['url'], sub, reddit_post_id)


def check_subs(subs):
    for sub in subs:
        posts = call_reddit(sub)
        check_posts(posts)
        time.sleep(2.1) # no more than 30 calls a min


def is_post_unsaved(reddit_post_id):
    result = db_table_posts.search(Query().reddit_post_id == reddit_post_id)
    return len(result) < 1


def is_author_unsaved(author):
    result = db_table_authors.search(Query().author == author)
    return len(result) < 1


def db_add_author(author):
    new_author_obj = {
        'author': author,
        'posts': []
    }
    db_table_authors.insert(new_author_obj)


def db_add_post(author, title, filenames, sub, reddit_post_id):
    post_obj = {
        'title': title,
        'author': author,
        'filename': filenames,
        'sub': sub,
        'reddit_post_id': reddit_post_id
    }

    post_obj_id = db_table_posts.insert(post_obj)

    target_author_obj_list = db_table_authors.search(Query().author == author)
    if len(target_author_obj_list) > 0:
        posts = target_author_obj_list[0]['posts']
        posts.append(post_obj_id)
        db_table_authors.update({'posts': posts}, Query().author == author)
    else:
        print('ERROR: author not found, impossibru!?')


def create_author(author):
    db_add_author(author)
    author_dir_path = '{0}/{1}'.format(DATA_DIR, author)
    os.mkdir(author_dir_path)


def extract_file_from_url(file_url):
    return file_url.split('/')[-1]


def extract_extension(filename):
    return filename.split('.')[-1]


def print_save_info(sub, author, title):
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print("{0} | {3} | {1} - {2}".format(current_time, author, title, sub))


def save_gallery(author, title, data):
    if 'media_metadata' not in data:
        print('missing media metadata')

    print_save_info(data['subreddit'], author, title)

    media_metadata = data['media_metadata']

    filenames = []
    high_res_urls = []
    thumb_urls = []

    for file in media_metadata:
        # sometimes there are failed objects in this array. not sure why.
        if media_metadata[file]['status'] == 'failed':
            continue

        high_res_url = media_metadata[file]['p'][-1]['u']
        high_res_url = high_res_url.replace('amp;', '')
        high_res_urls.append(high_res_url)

        thumb_url = media_metadata[file]['p'][0]['u']
        thumb_url = thumb_url.replace('amp;', '')
        thumb_urls.append(thumb_url)

        filename = high_res_url.split('?')[0].split('/')[-1]
        filenames.append(filename)

    download_gallery(high_res_urls, thumb_urls, author, filenames)
    db_add_post(author, title, filenames, data['subreddit'], data['id'])


def save_post(author, title, thumb_url, file_url, sub, reddit_post_id):
    print_save_info(sub, author, title)

    file_name = extract_file_from_url(file_url)
    db_add_post(author, title, [file_name], sub, reddit_post_id)
    if supported_file_type(extract_extension(file_name)):
        thumb_name = 'thumb-{0}'.format(file_name)
        download_file(file_url, thumb_url, author, file_name, thumb_name)


def save_video_post(author, title, data):
    print_save_info(data['subreddit'], author, title)

    file_name = extract_file_from_url(data['url'])
    filename_w_ext = '{0}.mp4'.format(file_name)
    dl_url = data['secure_media']['reddit_video']['fallback_url']
    db_add_post(author, title, [filename_w_ext], data['subreddit'], data['id'])
    thumb_ext = data['thumbnail'].split('.')[-1].lower()
    if thumb_ext != '':
        thumb_name = 'thumb-{0}.{1}'.format(file_name, thumb_ext)
    else:
        thumb_name = 'GENERATE_ME'
    download_file(dl_url, data['thumbnail'], author, filename_w_ext, thumb_name)


def supported_file_type(ext):
    extensions = ['jpg', 'jpeg', 'png']
    if ext.lower() in extensions:
        return True
    return False


def download_gallery(urls, thumb_urls, author, filenames):
    i = 0
    for url in urls:
        thumb_name = 'thumb-{0}'.format(filenames[i])
        thumb_dest_path = '{0}/{1}/{2}'.format(DATA_DIR, author, thumb_name)

        dest_path = '{0}/{1}/{2}'.format(DATA_DIR, author, filenames[i])

        wget_string = "wget --quiet --continue --tries=1 --no-clobber '{0}' --output-document={1}"
        command_a = wget_string.format(url, dest_path)
        command_b = wget_string.format(thumb_urls[i], thumb_dest_path)

        status_a = subprocess.run(command_a, shell=True)
        time.sleep(2.1)  # no more than 30 calls a min
        status_b = subprocess.run(command_b, shell=True)
        i += 1


# def gen_img_thumbnail(path):
#     size = 120, 120
#     with Image.open(path) as im:
#         im.thumbnail(size)
#         im.save(path + "-thumb" + ".jpg")


def gen_vid_thumbnail(input_path, author):
    path_no_ext = os.path.splitext(input_path)[0]
    filename_only = path_no_ext.split('/')[-1]
    filename_w_ext = 'thumb-{0}.jpg'.format(filename_only)

    output_path = '{0}/{1}/{2}'.format(DATA_DIR, author, filename_w_ext)
    subprocess.call(
        [
            'ffmpeg',
            '-hide_banner',
            '-loglevel',
            'error',
            '-i',
            input_path,
            '-ss',
            '00:00:00.000',
            '-vframes',
            '1',
            output_path
        ]
    )


def download_file(file_url, thumb_url, author, file_name, thumb_name):
    dest_path = '{0}/{1}/{2}'.format(DATA_DIR, author, file_name)
    thumb_dest_path = '{0}/{1}/{2}'.format(DATA_DIR, author, thumb_name)

    wget_string = 'wget --quiet --continue --tries=1 --no-clobber {0} --output-document={1}'

    command_a = wget_string.format(file_url, dest_path)
    if thumb_url != '' and thumb_name != 'GENERATE_ME':
        command_b = wget_string.format(thumb_url, thumb_dest_path)
        out_b = subprocess.run(command_b, shell=True)

    time.sleep(2.1)  # no more than 30 calls a min
    out_a = subprocess.run(command_a, shell=True)

    if thumb_name == 'GENERATE_ME':
        gen_vid_thumbnail(dest_path, author)


def get_latest_posts():
    db_length = len(db_table_posts)
    target_size = 25
    if db_length < 25:
        target_size = db_length
    i = 0
    latest_posts = []

    while i != target_size or i < 1:
        latest_posts.append(db_table_posts.get(doc_id=(db_length - i)))
        i += 1

    return latest_posts


def print_info(subreddits):
    print("Welcome to RedArc Ingest Local")
    print("written by klosure (https://gitlab.com/klosure/redarc-ingest-local)")
    print("ISC Licensed")
    print("")
    print("Data save location: {0}".format(DATA_DIR))
    print("")
    print("Monitoring Subreddits: {0}".format(subreddits))
    print("...")


def main():
    subreddits = load_subreddits()
    print_info(subreddits)

    while True:
        check_subs(subreddits)
        latest_posts = get_latest_posts()


if __name__ == "__main__":
    main()

#!/bin/sh

# Simple install/setup script

# Check if we've setup the virtual env for this project yet (first run)
if [ -d './venv' ] ; then
  python -m venv ./venv
  source ./venv/bin/activate
  pip install -r requirements.txt
else
  # second run
  echo "Looks like you've already got a virtual environment setup in this dir under ./venv"
fi

exit 0

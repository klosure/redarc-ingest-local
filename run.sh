#!/bin/sh

# Simple runner script

if [ -d './venv' ] ; then
  echo "Run setup.sh first..."
else
  source ./venv/bin/activate
  ./venv/bin/python ./red.py
fi

exit 0

# RedArc Ingest (Local)

[![License](https://is.gd/GbWFMI)](https://opensource.org/licenses/ISC)

This is a Python Backend service for RedArc that is meant to be run as a local script. Useful if you don't want your data to live in the cloud (i.e S3). This uses TinyDB to store info and Pillow to process images into thumbnails.


## Requirements

+ POSIX Shell
+ Python 3.8 (or greater)
+ wget
+ FFMpeg (optional)

## Install
```bash
git clone https://gitlab.com/klosure/redarc-ingest-local.git
cd redarc-ingest-local

./setup.sh

echo "bulldogs" > ./subreddits.conf
```

## Running
```bash
./run.sh
```

TODO:

+ only create author if post is to be saved
+ add thumbs dir
+ test PIL gen thumbnail
+ split users into a-z folders
+ video support (non-reddit)
+ imgur gallery support
+ wait on download process to return to gen thumb
+ add logging